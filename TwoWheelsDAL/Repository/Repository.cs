﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using TwoWheelsDAL.Infrastructure;
using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.Repository
{
    public class Repository<TEntity> : IDisposable, IRepository<TEntity> where TEntity : IEntityBase
    {
        public IMongoCollection<TEntity> Collection { get; set; }

        public Repository(ConnectionFactory connectionFactory)
        {
            Collection = connectionFactory.Db.GetCollection<TEntity>(typeof(TEntity).Name);
            EnsureUniqueIndexes();
        }

        private void EnsureUniqueIndexes()
        {
            //// implement if needed
        }

        public async Task<List<TEntity>> GetAll()
        {
            return await Collection.Find(x => true).ToListAsync();
        }

        public async Task<TEntity> Get(string id)
        {
            return await Collection.Find(x => x.ObjectId.Equals(id)).FirstOrDefaultAsync();
        }

        public async Task<List<TEntity>> GetObjectsByIds(List<string> ids)
        {
            var list = new List<TEntity>();
            var all = await this.GetAll();

            if (all == null || all.Count == 0 || ids == null || ids.Count == 0)
            {
                return null;
            }

            foreach (var id in ids)
            {
                list.Add(await Get(id));
            }

            return list;
        }

        public async Task<TEntity> Add(TEntity entity)
        {
            if (string.IsNullOrWhiteSpace(entity.ObjectId))
            {
                entity.ObjectId = ObjectId.GenerateNewId().ToString();
            }

            await Collection.InsertOneAsync(entity);

            return entity;
        }

        public async Task<DeleteResult> Delete(TEntity entity)
        {
            return await Collection.DeleteOneAsync(x => x.ObjectId.Equals(entity.ObjectId));
        }

        public async Task<DeleteResult> Delete(string objectId)
        {
            return await Collection.DeleteOneAsync(x => x.ObjectId.Equals(objectId));
        }

        public async Task<List<TEntity>> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return await Collection.Find(predicate).ToListAsync();
        }

        public async Task<TEntity> Update(Expression<Func<TEntity, bool>> predicate, TEntity entity)
        {
            return await Collection.FindOneAndReplaceAsync(
                predicate,
                entity,
                new FindOneAndReplaceOptions<TEntity> { ReturnDocument = ReturnDocument.After });
        }

        public void Dispose()
        {
        }
    }
}