﻿using System.Configuration;
using MongoDB.Driver;

namespace TwoWheelsDAL.Infrastructure
{
    public class ConnectionFactory
    {
        private readonly IMongoDatabase _db;
            
        public ConnectionFactory()
        {
            var remoteConnectionString = ConfigurationManager.ConnectionStrings["Remote"].ConnectionString;
            var localConnectionString = ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
            

            var client = new MongoClient(remoteConnectionString);
            _db = client.GetDatabase("TwoWheels");
        }

        public IMongoDatabase Db
        {
            get
            {
                if (_db == null) throw new MongoConnectionException(null, "Couldn't get database");
                return _db;
            }
        }
    }
}