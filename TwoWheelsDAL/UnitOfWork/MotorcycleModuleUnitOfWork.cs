﻿using System.Collections.Generic;
using System.Linq;

using TwoWheelsDAL.Models.MotorcycleModels;

namespace TwoWheelsDAL.UnitOfWork
{
    public class MotorcycleModuleUnitOfWork
    {
        public float RecalculateAverageFuelConsumption(List<Refuel> refuelings)
        {
            var mileage = refuelings.Max(x => x.CurrentMileage) - refuelings.Min(x => x.CurrentMileage);
            var fuel = refuelings.Sum(x => x.FuelCapacity) - refuelings.OrderByDescending(x => x.Date).Last().FuelCapacity;

            return fuel/(mileage/100);
        }

        public float RecalculateLastTankFuelConsumption(List<Refuel> refuelings)
        {
            var mileage = refuelings.Max(x => x.CurrentMileage)
                          - refuelings.OrderByDescending(x => x.CurrentMileage)
                              .Skip(1)
                              .Take(1)
                              .FirstOrDefault()
                              .CurrentMileage;
            var fuel = refuelings.OrderByDescending(x => x.CurrentMileage).FirstOrDefault().FuelCapacity;

            return fuel / ((float)mileage / 100);
        }

        public List<OperatingPart> RecalculateOperatingPartUsage(List<OperatingPart> operatingParts, Refuel refuel)
        {
            foreach (var operatingPart in operatingParts)
            {
                operatingPart.UsePercentage = (float)(refuel.CurrentMileage - operatingPart.LastChange)
                                              / operatingPart.LifeSpanInKilometers;
            }

            return operatingParts;
        }
    }
}