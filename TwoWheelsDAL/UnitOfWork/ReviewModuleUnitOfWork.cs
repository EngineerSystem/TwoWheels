﻿using System.Collections.Generic;

using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.UnitOfWork
{
    public class ReviewModuleUnitOfWork : UnitOfWork
    {
        public IReviewable AddReviewToObject(List<IReviewable> reviewableObjects, string objectId, string reviewId)
        {
            var reviewedObject = reviewableObjects.Find(x => x.ObjectId.Equals(objectId));

            if (reviewedObject == null)
            {
                return null;
            }

            if (reviewedObject.Reviews == null)
            {
                reviewedObject.Reviews = new List<string>();
            }

            reviewedObject.Reviews.Add(reviewId);

            return reviewedObject;
        }
    }
}