﻿using FluentValidation;

using TwoWheelsDAL.Models.MotorcycleModels;

namespace TwoWheelsDAL.BindingModels.MotorcycleModule
{
    public class AddRefuelingBindingModel
    {
        public string UserMotorcycleId { get; set; }
        public Refuel Refuel { get; set; }
    }

    public class AddRefuelingBindingModelValidator : AbstractValidator<AddRefuelingBindingModel>
    {
        public AddRefuelingBindingModelValidator()
        {
            RuleFor(x => x.Refuel.CurrentMileage).NotEmpty();
            RuleFor(x => x.UserMotorcycleId).NotEmpty();

            
        }
    }
}