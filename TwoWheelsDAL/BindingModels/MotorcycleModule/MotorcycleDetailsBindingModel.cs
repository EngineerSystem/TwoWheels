﻿using System.Collections.Generic;
using TwoWheelsDAL.Models;
using TwoWheelsDAL.Models.MotorcycleModels;

namespace TwoWheelsDAL.BindingModels.MotorcycleModule
{
    public class MotorcycleDetailsBindingModel
    {
        public string UserMotorcycleId { get; set; }
        public string Name { get; set; }
        public int CurrentMileage { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public string Type { get; set; }
        public int ProductionYear { get; set; }
        public int InitialMileage { get; set; }
        public float AverageFuelConsumption { get; set; }
        public List<OperatingPart> OperatingParts { get; set; }
    }
}