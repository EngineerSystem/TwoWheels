﻿using System.Collections.Generic;
using TwoWheelsDAL.Models;
using TwoWheelsDAL.Models.MotorcycleModels;

namespace TwoWheelsDAL.BindingModels.MotorcycleModule
{
    public class ManageOperatingPartsBindingModel
    {
        public List<OperatingPart> OperatingParts { get; set; }
        public string UserMotorcycleId { get; set; }
    }
}