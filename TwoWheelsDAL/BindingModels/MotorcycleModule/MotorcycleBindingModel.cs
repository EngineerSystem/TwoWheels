﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TwoWheelsDAL.BindingModels.MotorcycleModule
{
    public class MotorcycleBindingModel
    {
        public string UserMotorcycleId { get; set; }
        public string Name { get; set; }
        public int CurrentMileage { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public string Type { get; set; }
        public int ProductionYear { get; set; }
    }
}