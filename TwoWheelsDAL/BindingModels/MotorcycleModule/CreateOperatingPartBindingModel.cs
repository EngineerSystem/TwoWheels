﻿using System.Collections.Generic;
using TwoWheelsDAL.Models;
using TwoWheelsDAL.Models.MotorcycleModels;

namespace TwoWheelsDAL.BindingModels.MotorcycleModule
{
    public class CreateOperatingPartBindingModel
    {
        public string UserMotorcycleId { get; set; }
        public OperatingPart OperatingPart { get; set; }
    }
}