﻿namespace TwoWheelsDAL.BindingModels.MotorcycleModule
{
    public class RefuelingSummaryBindingModel
    {
        public string UserMotorcycleId { get; set; }
        public string UserMotorcycleName { get; set; }
        public float AverageFuelConsumption { get; set; }
    }
}