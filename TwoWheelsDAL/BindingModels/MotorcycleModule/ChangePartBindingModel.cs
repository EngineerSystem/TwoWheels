﻿namespace TwoWheelsDAL.BindingModels.MotorcycleModule
{
    public class ChangePartBindingModel
    {
        public string UserMotorcycleId { get; set; }
        public string OperatingPartName { get; set; }
        public int CurrentMileage { get; set; }
    }
}