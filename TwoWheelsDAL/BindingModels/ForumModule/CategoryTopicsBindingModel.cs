﻿using System.Collections.Generic;
using TwoWheelsDAL.Models.ForumModels;

namespace TwoWheelsDAL.BindingModels.ForumModule
{
    public class CategoryTopicsBindingModel
    {
        public List<Topic> Topics { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public List<SubCategory> SubCategories { get; set; }
    }
}