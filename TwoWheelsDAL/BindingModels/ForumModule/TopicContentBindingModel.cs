﻿using System.Collections.Generic;
using TwoWheelsDAL.Models.ForumModels;

namespace TwoWheelsDAL.BindingModels.ForumModule
{
    public class TopicContentBindingModel
    {
        public string Username { get; set; }
        public Topic Topic { get; set; }
        public List<PostWithAuthor> Posts { get; set; }
    }

    public class PostWithAuthor
    {
        public Post Post { get; set; }
        public string Username { get; set; }
    }
}