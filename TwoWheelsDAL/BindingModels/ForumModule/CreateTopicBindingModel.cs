﻿using FluentValidation;

using TwoWheelsDAL.Models.ForumModels;

namespace TwoWheelsDAL.BindingModels.ForumModule
{
    public class CreateTopicBindingModel
    {
        public string CategoryId { get; set; }
        public Topic Topic { get; set; }
        public string PostContent { get; set; }
        public string Tags { get; set; }
    }

    public class CreateTopicBindingModelValidator : AbstractValidator<CreateTopicBindingModel>
    {
        public CreateTopicBindingModelValidator()
        {
            RuleFor(x => x.CategoryId).NotEmpty().WithMessage("You have to have category!");
            RuleFor(x => x.PostContent)
                .NotEmpty()
                .MinimumLength(10)
                .WithMessage("You must enter the post content, and it must have minimum 10 signs");
            RuleFor(x => x.Topic.UserId).NotEmpty();
            RuleFor(x => x.Topic.Name).NotEmpty();
            RuleFor(x => x.CategoryId).NotEmpty();
        }
    }
}