﻿using System.Collections.Generic;

using TwoWheelsDAL.Models.ForumModels;

namespace TwoWheelsDAL.BindingModels.ForumModule
{
    public class SearchedTopicsBindingModel
    {
        public string SearchedTag { get; set; }
        public List<Topic> Topics { get; set; }
    }
}