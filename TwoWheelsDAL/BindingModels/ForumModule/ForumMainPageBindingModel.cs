﻿using System.Collections.Generic;
using TwoWheelsDAL.Models.ForumModels;

namespace TwoWheelsDAL.BindingModels.ForumModule
{
    public class ForumMainPageBindingModel
    {
        public List<CategoryWithSubcategories> List { get; set; }
    }

    public class CategoryWithSubcategories
    {
        public Category Category { get; set; }
        public List<SubCategory> SubCategories { get; set; }
    }
}