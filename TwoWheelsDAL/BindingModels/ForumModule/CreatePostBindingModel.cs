﻿using FluentValidation;

using TwoWheelsDAL.Models.ForumModels;

namespace TwoWheelsDAL.BindingModels.ForumModule
{
    public class CreatePostBindingModel
    {
        public Post Post { get; set; }
        public string TopicId { get; set; }
    }

    public class CreatePostBindingModelValidator : AbstractValidator<CreatePostBindingModel>
    {
        public CreatePostBindingModelValidator()
        {
            RuleFor(x => x.Post.Content)
                .NotEmpty()
                .MinimumLength(10)
                .WithMessage("You must enter the post content, and it must have minimum 10 signs");
            RuleFor(x => x.Post.UserId).NotEmpty();
            RuleFor(x => x.TopicId).NotEmpty();
        }
    }
}