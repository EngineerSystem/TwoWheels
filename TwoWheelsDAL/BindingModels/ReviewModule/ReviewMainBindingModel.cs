﻿using System.Collections.Generic;
using TwoWheelsDAL.Models.ReviewModels;

namespace TwoWheelsDAL.BindingModels.ReviewModule
{
    public class ReviewMainBindingModel
    {
        public List<ReviewMain> Reviews { get; set; }
    }

    public class ReviewMain
    {
        public string Mark { get; set; }
        public string Model { get; set; }
        public string ObjectId { get; set; }
        public Review Review { get; set; }
    }
}