﻿using System.Collections.Generic;
using TwoWheelsDAL.Models.ReviewModels;

namespace TwoWheelsDAL.BindingModels.ReviewModule
{
    public class ObjectsToReviewBindingModel
    {
        public List<ObjectToReview> Objects { get; set; }
    }

    public class ObjectToReview
    {
        public string ObjectId { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public GearCategory GearCategory { get; set; }
    }
}