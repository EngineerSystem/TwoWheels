﻿using TwoWheelsDAL.Models.ReviewModels;

namespace TwoWheelsDAL.BindingModels.ReviewModule
{
    public class CreateReviewBindingModel
    {
        public Review Review { get; set; }
        public string ObjectId { get; set; }
        public string Name { get; set; }
    }
}