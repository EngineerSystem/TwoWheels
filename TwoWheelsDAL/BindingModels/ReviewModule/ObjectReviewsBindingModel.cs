﻿using System.Collections.Generic;
using TwoWheelsDAL.Models.ReviewModels;

namespace TwoWheelsDAL.BindingModels.ReviewModule
{
    public class ObjectReviewsBindingModel
    {
        public float Average { get; set; }
        public List<Review> Reviews { get; set; }
        public string ObjectId { get; set; }
        public string Name { get; set; }
    }
}