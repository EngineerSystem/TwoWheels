﻿using System.Collections.Generic;
using TwoWheelsDAL.Models.ReviewModels;

namespace TwoWheelsDAL.BindingModels.ReviewModule
{
    public class CreateGearBindingModel
    {
        public Gear Gear { get; set; }
        public List<GearCategory> GearCategories { get; set; }
    }
}