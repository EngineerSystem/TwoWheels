﻿using System.Collections.Generic;

namespace TwoWheelsDAL.Interfaces
{
    public interface IReviewable : IEntityBase
    {
        string Mark { get; set; }
        string Product { get; set; }
        List<string> Reviews { get; set; }
    }
}