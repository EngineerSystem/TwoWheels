﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TwoWheelsDAL.Interfaces
{
    public interface IEntityBase
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        string ObjectId { get; set; }
    }
}