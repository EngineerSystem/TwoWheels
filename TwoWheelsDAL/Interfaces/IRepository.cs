﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace TwoWheelsDAL.Interfaces
{ 
    public interface IRepository<TEntity> where TEntity : IEntityBase
    {
        Task<List<TEntity>> GetAll();
        Task<TEntity> Get(string id);
        Task<List<TEntity>> GetObjectsByIds(List<string> ids);
        Task<TEntity> Add(TEntity entity);
        Task<DeleteResult> Delete(TEntity entity);
        Task<DeleteResult> Delete(string objectId);
        Task<List<TEntity>> Find(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> Update(Expression<Func<TEntity, bool>> predicate, TEntity entity);
    }
}