﻿using System.Collections.Generic;

namespace TwoWheelsDAL.Interfaces
{
    public interface ICategory : IEntityBase
    {
        string Name { get; set; }

        List<string> Topics { get; set; }
    }
}