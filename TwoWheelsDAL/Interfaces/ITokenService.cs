﻿using TwoWheelsDAL.Models.AccountModels;

namespace TwoWheelsDAL.Interfaces
{
    public interface ITokenService
    {
        void SaveRefreshToken(RefreshTokenModel refreshToken);
        RefreshTokenModel GetRefreshToken(string hashedToken);
        void RemoveRefreshToken(string hashedToken);
        void RemoveExpiredRefreshTokens(string userName);
    }
}