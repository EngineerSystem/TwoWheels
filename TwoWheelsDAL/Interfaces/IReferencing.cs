﻿using System.Collections.Generic;

namespace TwoWheelsDAL.Interfaces
{
    public interface IReferencing : IEntityBase
    {
        List<string> List { get; set; }
    }
}