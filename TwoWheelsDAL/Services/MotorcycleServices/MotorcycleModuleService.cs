﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using TwoWheelsDAL.BindingModels.MotorcycleModule;
using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Models.MotorcycleModels;
using TwoWheelsDAL.UnitOfWork;

namespace TwoWheelsDAL.Services.MotorcycleServices
{
    public class MotorcycleModuleService : IService
    {
        private readonly IRepository<MotorcycleModel> _motorcycleModelsRepository;
        private readonly IRepository<UserMotorcycle> _userMotorcyclesRepository;
        private readonly IRepository<Refuel> _refuelingRepository;
        private readonly MotorcycleModuleUnitOfWork _motorcycleUnitOfWork;

        public MotorcycleModuleService(IRepository<MotorcycleModel> motorcycleModelsRepository,
            IRepository<UserMotorcycle> userMotorcyclesRepository, IRepository<Refuel> refuelingRepository)
        {
            _motorcycleModelsRepository = motorcycleModelsRepository;
            _userMotorcyclesRepository = userMotorcyclesRepository;
            _refuelingRepository = refuelingRepository;
            this._motorcycleUnitOfWork = new MotorcycleModuleUnitOfWork();
        }

        public async Task<List<MotorcycleBindingModel>> GetUserMotorcycles(string userId)
        {
            var userMotorcycles = await _userMotorcyclesRepository.Find(x => x.UserId.Equals(userId));
            List<MotorcycleBindingModel> listToReturn = new List<MotorcycleBindingModel>();

            if (userMotorcycles.Count <= 0) return listToReturn;
            foreach (var userMotorcycle in userMotorcycles)
            {
                var model = await _motorcycleModelsRepository.Get(userMotorcycle.MotorcycleModelId);

                listToReturn.Add(new MotorcycleBindingModel
                {
                    Name = userMotorcycle.Name,
                    UserMotorcycleId = userMotorcycle.ObjectId,
                    CurrentMileage = userMotorcycle.CurrentMileage,
                    Mark = model.Mark,
                    Model = model.Product,
                    ProductionYear = model.ProductionYear,
                    Type = model.Type.ToString()
                });
            }

            return listToReturn;
        }

        public async Task<string> GetUserMotorcycleType(string id)
        {
            var userMotorcycle = await _userMotorcyclesRepository.Get(id);

            var motorcycleModel = await _motorcycleModelsRepository.Get(userMotorcycle.MotorcycleModelId);

            return motorcycleModel.Type.ToString();
        }

        public async Task<string> GetMotorcycleModelType(string id)
        {
            var model = await _motorcycleModelsRepository.Get(id);

            return model.Type.ToString();
        }

        public async Task<bool> CreateOperatingPart(CreateOperatingPartBindingModel model)
        {
            var userMotorcycle = await _userMotorcyclesRepository.Get(model.UserMotorcycleId);

            if (model.OperatingPart == null)
            {
                return false;
            }

            userMotorcycle?.OperatingParts.Add(new OperatingPart(model.OperatingPart.Name,
                model.OperatingPart.LifeSpanInKilometers, userMotorcycle.CurrentMileage));

            return await UpdateUserMotorcycle(userMotorcycle);
        }

        public async Task<bool> UpdateUserMotorcycle(UserMotorcycle userMotorcycle)
        {
            var updateResult =
                await _userMotorcyclesRepository.Update(x => x.ObjectId.Equals(userMotorcycle.ObjectId), userMotorcycle);

            return !updateResult.ObjectId.Equals(string.Empty);
        }

        public async Task<float> AddRefueling(AddRefuelingBindingModel model)
        {
            var refuel = new Refuel(
                model.Refuel.CurrentMileage,
                model.Refuel.FuelUnitPrice,
                model.Refuel.RefuelingPrice,
                model.Refuel.FuelCapacity);

            var addRefuelResult = await _refuelingRepository.Add(refuel);

            var userMotorcycle = await _userMotorcyclesRepository.Get(model.UserMotorcycleId);
            userMotorcycle.CurrentMileage = model.Refuel.CurrentMileage;
            userMotorcycle.RefuelingIds.Add(addRefuelResult.ObjectId);

            userMotorcycle.OperatingParts =
                this._motorcycleUnitOfWork.RecalculateOperatingPartUsage(userMotorcycle.OperatingParts, refuel);

            if (userMotorcycle.RefuelingIds.Count > 1)
            {
                var refuelings = await this._refuelingRepository.GetObjectsByIds(userMotorcycle.RefuelingIds);

                userMotorcycle.AverageFuelConsumption =
                    _motorcycleUnitOfWork.RecalculateAverageFuelConsumption(refuelings);

                userMotorcycle.LastTankFuelConsumption =
                    _motorcycleUnitOfWork.RecalculateLastTankFuelConsumption(refuelings);

                addRefuelResult.LastFuelConsumption = userMotorcycle.LastTankFuelConsumption;

                await _refuelingRepository.Update(x => x.ObjectId.Equals(addRefuelResult.ObjectId), addRefuelResult);
            }

            await UpdateUserMotorcycle(userMotorcycle);

            return userMotorcycle.AverageFuelConsumption;
        }

        public async Task<List<Refuel>> GetRefuelings(string userMotorcycleId)
        {
            var userMotorcycle = await this._userMotorcyclesRepository.Get(userMotorcycleId);

            return await this._refuelingRepository.GetObjectsByIds(userMotorcycle.RefuelingIds);
        }

        public async Task<RefuelingSummaryBindingModel> GetRefuelingSummary(string userMotorcycleId)
        {
            var userMotorcycle = await this._userMotorcyclesRepository.Get(userMotorcycleId);

            return new RefuelingSummaryBindingModel
            {
                UserMotorcycleId = userMotorcycleId,
                UserMotorcycleName = userMotorcycle.Name,
                AverageFuelConsumption = userMotorcycle.AverageFuelConsumption
            };
        }

        public async Task<MotorcycleDetailsBindingModel> GetMotorcycleDetails(string userMotorcycleId)
        {
            var userMotorcycle = await _userMotorcyclesRepository.Get(userMotorcycleId);
            var motorcycleModel = await _motorcycleModelsRepository.Get(userMotorcycle.MotorcycleModelId);
            var parts = userMotorcycle.OperatingParts;

            foreach (var part in parts)
            {
                part.UsePercentage *= 100;
                part.UsePercentage = (float)Math.Round(part.UsePercentage, 0);
            }


            return new MotorcycleDetailsBindingModel
            {
                OperatingParts = parts,
                Model = motorcycleModel.Product,
                Mark = motorcycleModel.Mark,
                UserMotorcycleId = userMotorcycle.ObjectId,
                CurrentMileage = userMotorcycle.CurrentMileage,
                Name = userMotorcycle.Name,
                Type = motorcycleModel.Type.ToString(),
                ProductionYear = motorcycleModel.ProductionYear,
                AverageFuelConsumption = userMotorcycle.AverageFuelConsumption,
                InitialMileage = userMotorcycle.InitialMileage
            };
        }

        public async Task<UserMotorcycle> CreateUserMotorcycle(UserMotorcycle model)
        {
            var userMotorcycle = model;
            var type = await GetMotorcycleModelType(model.MotorcycleModelId);
            MotorcycleType enumType = MotorcycleType.None;

            Enum.TryParse<MotorcycleType>(type, true, out enumType);

            userMotorcycle.OperatingParts = new List<OperatingPart>
            {
                { new OperatingPart("Engine oil", enumType, model.InitialMileage) },
                { new OperatingPart("Tires", enumType, model.InitialMileage) },
                { new OperatingPart("Chain", enumType, model.InitialMileage) }
            };

            var result = await _userMotorcyclesRepository.Add(userMotorcycle);

            return result;
        }

        public async Task<bool> UpdateOperatingParts(ManageOperatingPartsBindingModel model)
        {
            var userMotorcycle = await _userMotorcyclesRepository.Get(model.UserMotorcycleId);
            userMotorcycle.OperatingParts = model.OperatingParts;

            return await UpdateUserMotorcycle(userMotorcycle);
        }

        public async Task<ManageOperatingPartsBindingModel> GetUserMotorcycleOperatingParts(string userMotorcycleId)
        {
            var userMotorcycle = await _userMotorcyclesRepository.Get(userMotorcycleId);

            return new ManageOperatingPartsBindingModel
            {
                UserMotorcycleId = userMotorcycleId,
                OperatingParts = userMotorcycle.OperatingParts
            };
        }

        public async Task<bool> ChangePart(ChangePartBindingModel model)
        {
            var userMotorcycle = await _userMotorcyclesRepository.Get(model.UserMotorcycleId);
            var operatingPart =
                userMotorcycle.OperatingParts.FirstOrDefault(
                    x => x.Name.Equals(model.OperatingPartName, StringComparison.CurrentCultureIgnoreCase));

            if (operatingPart == null)
            {
                return false;
            }

            operatingPart.LastChange = model.CurrentMileage;
            operatingPart.UsePercentage = 0;

            return await UpdateUserMotorcycle(userMotorcycle);
        }
    }
}