﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;
using TwoWheelsDAL.BindingModels.ForumModule;
using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Models.AccountModels;
using TwoWheelsDAL.Models.ForumModels;
using TwoWheelsDAL.Services.MotorcycleServices;

namespace TwoWheelsDAL.Services.ForumServices
{
    public class ForumModuleService : IService
    {
        private readonly IRepository<Topic> _topicRepository;
        private readonly IRepository<Post> _postRepository;
        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<SubCategory> _subCategoryRepository;
        private readonly IRepository<ApplicationUser> _userRepository;

        public ForumModuleService(IRepository<Topic> topicRepository, IRepository<Post> postRepository,
            IRepository<Category> categoryRepository,
            IRepository<SubCategory> subCategoryRepository, IRepository<ApplicationUser> userRepository)
        { 
            _topicRepository = topicRepository;
            _postRepository = postRepository;
            _categoryRepository = categoryRepository;
            _subCategoryRepository = subCategoryRepository;
            this._userRepository = userRepository;
        }

        public async Task<ForumMainPageBindingModel> GetForumMailPageBindingModel()
        {
            var categories = await _categoryRepository.GetAll();
            List<CategoryWithSubcategories> modelList = new List<CategoryWithSubcategories>();

            foreach (var category in categories)
            {
                var element = new CategoryWithSubcategories
                {
                    Category = category,
                    SubCategories = await _subCategoryRepository.GetObjectsByIds(category.SubCategories)
                };

                modelList.Add(element);
            }

            return new ForumMainPageBindingModel{List = modelList};
        }

        public async Task<TopicContentBindingModel> GetTopicContent(string topicId)
        {
            var topic = await _topicRepository.Get(topicId);
            if (topic.UserId == null)
            {
                return null;
            }
            var user = await this._userRepository.Get(topic.UserId);
            var model = new TopicContentBindingModel
            {
                Username = user.Email,
                Topic = topic,
                Posts = new List<PostWithAuthor>()
            };

            var posts = await _postRepository.GetObjectsByIds(topic.Posts);

            foreach (var post in posts)
            {
                if (post.UserId == null)
                {
                    continue;
                }
                var postUser = await this._userRepository.Get(post.UserId);
                model.Posts.Add(new PostWithAuthor {Post = post, Username = postUser.Email});
            }

            return model;
        }

        public async Task<string> CreateTopic(CreateTopicBindingModel model)
        {
            var category = await DefineIfIdIsCategoryOrSubCategory(model.CategoryId);
            if (category == null)
            {
                return string.Empty;
            }

            var stringTags = model.Tags.Split(',').ToList();
            var topic = new Topic(model.Topic.Name, model.Topic.Description, model.Topic.UserId, stringTags);
            var topicCreateResult = await this._topicRepository.Add(topic);

            if (string.IsNullOrEmpty(topicCreateResult.ObjectId))
            {
                return string.Empty;
            }

            var createPostResult =
                await CreatePost(
                    new CreatePostBindingModel
                    {
                        TopicId = topicCreateResult.ObjectId,
                        Post = new Post(model.PostContent, model.Topic.UserId)
                    });

            category.Topics.Add(topicCreateResult.ObjectId);

            if (category is Category)
            {
                await this._categoryRepository.Update(x => x.ObjectId.Equals(category.ObjectId), (Category)category);
            }
            else
            {
                await this._subCategoryRepository.Update(x => x.ObjectId.Equals(category.ObjectId), (SubCategory)category);
            }

            return topic.ObjectId;
        }

        public async Task<CategoryTopicsBindingModel> GetCategoryTopics(string categoryId)
        {
            ICategory category = await DefineIfIdIsCategoryOrSubCategory(categoryId);
            List<string> topicsIds;
            var model = new CategoryTopicsBindingModel
            {
                CategoryId = categoryId,
                CategoryName = category.Name,
                Topics = new List<Topic>()
            };

            if (category is Category)
            {
                model.SubCategories =
                    await _subCategoryRepository.GetObjectsByIds((category as Category).SubCategories);
                topicsIds = category.Topics;
            }
            else
            {
                topicsIds = category.Topics;
            }

            model.Topics = await _topicRepository.GetObjectsByIds(topicsIds);
            
            return model;
        }

        public async Task<bool> CreatePost(CreatePostBindingModel model)
        {
            var result = await _postRepository.Add(new Post(model.Post.Content, model.Post.UserId));

            var topic = await this._topicRepository.Get(model.TopicId);
            topic.Posts.Add(result.ObjectId);

            var updateTopic = await this._topicRepository.Update(x => x.ObjectId.Equals(topic.ObjectId), topic);

            return !string.IsNullOrEmpty(result.ObjectId);
        }

        public async Task<ICategory> DefineIfIdIsCategoryOrSubCategory(string id)
        {
            var category = await _categoryRepository.Get(id);

            if (category != null)
            {
                return category;
            }

            return await this._subCategoryRepository.Get(id);
        }

        public async Task<bool> CreateCategory(ICategory model, string id)
        {
            ICategory categoryAddResult;

            if (id.Equals(string.Empty))
            {
                categoryAddResult = await this._categoryRepository.Add(new Category(model.Name));
            }
            else
            {
                var category = await this._categoryRepository.Get(id);
                categoryAddResult = await this._subCategoryRepository.Add(new SubCategory(model.Name));
                category.SubCategories.Add(categoryAddResult.ObjectId);
                var updateResult = await this._categoryRepository.Update(
                    x => x.ObjectId.Equals(category.ObjectId),
                    category);
            }

            return !categoryAddResult.ObjectId.Equals(string.Empty);
        }

        public async Task<SearchedTopicsBindingModel> SearchByTag(string tag)
        {
            var topics = await this._topicRepository.GetAll();
            var selected = new List<Topic>();

            foreach (var topic in topics)
            {
                selected.AddRange(from topicTag in topic.Tags where topicTag.Name.Equals(tag) select topic);
            }

            return new SearchedTopicsBindingModel
            {
                SearchedTag = tag,
                Topics = selected
            };
        }
    }
}