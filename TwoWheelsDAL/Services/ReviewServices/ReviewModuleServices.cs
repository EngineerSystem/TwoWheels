﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwoWheelsDAL.BindingModels.ReviewModule;
using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Models.MotorcycleModels;
using TwoWheelsDAL.Models.ReviewModels;
using TwoWheelsDAL.Services.MotorcycleServices;
using TwoWheelsDAL.UnitOfWork;

namespace TwoWheelsDAL.Services.ReviewServices
{
    public class ReviewModuleServices : IService
    {
        private readonly IRepository<Review> _reviewRepository;
        private readonly IRepository<GearCategory> _gearCategoryRepository;
        private readonly IRepository<Gear> _gearRepository;
        private readonly IRepository<MotorcycleModel> _motorcycleModelsRepository;
        private readonly ReviewModuleUnitOfWork _reviewUnitOfWork;

        public ReviewModuleServices(IRepository<Review> reviewRepository,
            IRepository<GearCategory> gearCategoryRepository, IRepository<Gear> gearRepository,
            IRepository<MotorcycleModel> motorcycleModelsRepository)
        {
            _reviewRepository = reviewRepository;
            _gearCategoryRepository = gearCategoryRepository;
            _gearRepository = gearRepository;
            _motorcycleModelsRepository = motorcycleModelsRepository;
            this._reviewUnitOfWork = new ReviewModuleUnitOfWork();
        }

        public async Task<ObjectsToReviewBindingModel> GetObjectsToReview()
        {
            var gearCategories = await _gearCategoryRepository.GetAll();

            var objectsToReview = new ObjectsToReviewBindingModel
            {
                Objects = new List<ObjectToReview>()
            };

            var reviewableObjects = new List<IReviewable>();
            reviewableObjects.AddRange(await this._motorcycleModelsRepository.GetAll());
            reviewableObjects.AddRange(await this._gearRepository.GetAll());

            foreach (var reviewable in reviewableObjects)
            {
                objectsToReview.Objects.Add(new ObjectToReview
                {
                    ObjectId = reviewable.ObjectId,
                    Mark = reviewable.Mark,
                    Model = reviewable.Product,
                    GearCategory = reviewable.GetType().Name.Equals("Gear") 
                        ? gearCategories.FirstOrDefault(x => x.ObjectId.Equals((reviewable as Gear).GearCategoryId))
                        : new GearCategory { Category = "Motorcycle"}
                });
            }

            return objectsToReview;
        }
        
        public async Task<ObjectReviewsBindingModel> GetObjectReviews(string id)
        {
            var returned = new ObjectReviewsBindingModel
            {
                ObjectId = id
            };

            var reviewableObjects = new List<IReviewable>();
            reviewableObjects.AddRange(await this._motorcycleModelsRepository.GetAll());
            reviewableObjects.AddRange(await this._gearRepository.GetAll());

            foreach (var reviewable in reviewableObjects)
            {
                if (!reviewable.ObjectId.Equals(id)) continue;

                returned.Name = $"{reviewable.Mark} {reviewable.Product}";
                returned.Reviews = await _reviewRepository.GetObjectsByIds(reviewable.Reviews);
                returned.Average = CalculateAverageStars(returned.Reviews);
                return returned;
            }

            return null;
        }

        private float CalculateAverageStars(List<Review> returnedReviews)
        {
            if (returnedReviews == null || returnedReviews.Count == 0)
            {
                return 0F;
            }

            return (float)returnedReviews.Select(x => x.Stars).Average();
        }

        public async Task<CreateGearBindingModel> GetCreateGearBindingModel()
        {
            var gearCategories = await _gearCategoryRepository.GetAll();
        
            var model = new CreateGearBindingModel
            {
                GearCategories = gearCategories
            };

            return model;
        }

        public async Task<ReviewMainBindingModel> GetIndexPageAsync()
        {
            var reviews =
                await _reviewRepository.GetAll();

            var lastWeekReviews = reviews.Where(x => x.Date - DateTime.Today < TimeSpan.FromDays(7));

            var model = new ReviewMainBindingModel
            {
                Reviews = new List<ReviewMain>()
            };

            var reviewableObjects = new List<IReviewable>();
            reviewableObjects.AddRange(await this._motorcycleModelsRepository.GetAll());
            reviewableObjects.AddRange(await this._gearRepository.GetAll());

            foreach (var review in lastWeekReviews)
            {
                foreach (var reviewable in reviewableObjects)
                {
                    if (reviewable.Reviews == null) continue;

                    if (reviewable.Reviews.Contains(review.ObjectId))
                    {
                        model.Reviews.Add(new ReviewMain
                        {
                            ObjectId = review.ObjectId,
                            Review = review,
                            Mark = reviewable.Mark,
                            Model = reviewable.Product
                        });
                    }
                }
            }

            return model;
        }

        public async Task<string> GetCategoryId(string name)
        {
            var category = await this._gearCategoryRepository.Find(x => x.Category.Equals(name));

            return category?.FirstOrDefault().ObjectId;
        }

        public async Task<bool> CreateReviewAsync(CreateReviewBindingModel model)
        {
            var reviewableObjects = new List<IReviewable>();
            reviewableObjects.AddRange(await this._motorcycleModelsRepository.GetAll());
            reviewableObjects.AddRange(await this._gearRepository.GetAll());

            var addReviewResult =
                await this._reviewRepository.Add(
                    new Review(model.Review.Stars, model.Review.Content, model.Review.UserId));

            var updatedObject = this._reviewUnitOfWork.AddReviewToObject(
                reviewableObjects,
                model.ObjectId,
                addReviewResult.ObjectId);

            object updateResult = null;

            if (updatedObject.GetType().Name.Equals("Gear"))
            {
                updateResult = await _gearRepository.Update(x => x.ObjectId.Equals(updatedObject.ObjectId), updatedObject as Gear);
            }
            else if(updatedObject.GetType().Name.Equals("MotorcycleModel"))
            {
                updateResult = await _motorcycleModelsRepository.Update(
                    x => x.ObjectId.Equals(updatedObject.ObjectId),
                    updatedObject as MotorcycleModel);
            }
            else
            {
                return false;
            }

            return updateResult != null;
        }
    }
}