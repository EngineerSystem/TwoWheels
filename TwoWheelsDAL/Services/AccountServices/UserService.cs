﻿using Microsoft.AspNet.Identity;
using TwoWheelsDAL.Models.AccountModels;

namespace TwoWheelsDAL.Services.AccountServices
{
    public class UserService
    {
        public ApplicationUserStore ApplicationUserStore { get; set; }
        public UserManager<ApplicationUser> UserManager { get; set; }

        public UserService(ApplicationUserStore applicationUserStore, UserManager<ApplicationUser> userManager)
        {
            ApplicationUserStore = applicationUserStore;
            UserManager = new UserManager<ApplicationUser>(ApplicationUserStore);
        }


    }
}