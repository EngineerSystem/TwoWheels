﻿using System.Linq;
using System.Threading.Tasks;
using AspNet.Identity.MongoDB;
using Microsoft.AspNet.Identity;
using MongoDB.Driver;
using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Models.AccountModels;
using IdentityUser = AspNet.Identity.MongoDB.IdentityUser;

namespace TwoWheelsDAL.Services.AccountServices
{
    public class ApplicationUserStore : IUserStore<ApplicationUser>, 
                                    IUserPasswordStore<ApplicationUser, string>, 
                                    IUserEmailStore<ApplicationUser, string>,
                                    IUserSecurityStampStore<ApplicationUser> 
    {
        private readonly IRepository<ApplicationUser> _userRepository;
        private readonly UserStore<IdentityUser> _userStore;

        public ApplicationUserStore(IRepository<ApplicationUser> userRepository)
        {
            _userRepository = userRepository;
            var collection = _userRepository.GetAll();
            _userStore = new UserStore<IdentityUser>(collection as IMongoCollection<IdentityUser>);
        }

        public async Task CreateAsync(ApplicationUser user)
        {
            await _userRepository.Add(user);
        }

        public async Task UpdateAsync(ApplicationUser user)
        {
            await _userRepository.Update(x => x.ObjectId.Equals(user.ObjectId), user);
        }

        public async Task DeleteAsync(ApplicationUser user)
        {
            await _userRepository.Delete(user);
        }

        public async Task<ApplicationUser> FindByIdAsync(string userId)
        {
            var found = await _userRepository.Find(x => x.Id.Equals(userId));

            return found.FirstOrDefault();
        }

        public async Task<ApplicationUser> FindByNameAsync(string userName)
        {
            var found = await _userRepository.Find(x => x.UserName.Equals(userName));

            return found.FirstOrDefault();
        }

        public void Dispose()
        {
            _userStore.Dispose();
        }

        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash)
        {
            var identityUser = ToIdentityUser(user);
            var task = _userStore.SetPasswordHashAsync(identityUser, passwordHash);
            SetApplicationUser(user, identityUser);

            return task;
        }

        public Task<string> GetPasswordHashAsync(ApplicationUser user)
        {
            var identityUser = ToIdentityUser(user);
            var task = _userStore.GetPasswordHashAsync(identityUser);
            SetApplicationUser(user, identityUser);

            return task;
        }

        public Task<bool> HasPasswordAsync(ApplicationUser user)
        {
            var identityUser = ToIdentityUser(user);
            var task = _userStore.HasPasswordAsync(identityUser);
            SetApplicationUser(user, identityUser);

            return task;
        }

        public async Task SetEmailAsync(ApplicationUser user, string email)
        {
            var identityUser = ToIdentityUser(user);
            await _userStore.SetEmailAsync(identityUser, email);
            SetApplicationUser(user, identityUser);
        }

        public async Task<string> GetEmailAsync(ApplicationUser user)
        {
            var identityUser = ToIdentityUser(user);
            var task = await _userStore.GetEmailAsync(identityUser);
            SetApplicationUser(user, identityUser);

            return task;
        }

        public async Task<bool> GetEmailConfirmedAsync(ApplicationUser user)
        {
            var identityUser = ToIdentityUser(user);
            var task = await _userStore.GetEmailConfirmedAsync(identityUser);
            SetApplicationUser(user, identityUser);

            return task;
        }

        public async Task SetEmailConfirmedAsync(ApplicationUser user, bool confirmed)
        {
            var identityUser = ToIdentityUser(user);
            await _userStore.SetEmailConfirmedAsync(identityUser, confirmed);
            SetApplicationUser(user, identityUser);
        }

        public async Task<ApplicationUser> FindByEmailAsync(string email)
        {
            var task = await _userRepository.Find(x => x.Email.Equals(email));

            return task.FirstOrDefault();
        }

        public Task SetSecurityStampAsync(ApplicationUser user, string stamp)
        {
            var identityUser = ToIdentityUser(user);
            var task = _userStore.SetSecurityStampAsync(identityUser, stamp);
            SetApplicationUser(user, identityUser);

            return task;
        }

        public Task<string> GetSecurityStampAsync(ApplicationUser user)
        {
            var identityUser = ToIdentityUser(user);
            var task = _userStore.GetSecurityStampAsync(identityUser);
            SetApplicationUser(user, identityUser);

            return task;
        }

        private ApplicationUser ToApplicationUser(IdentityUser user)
        {
            return new ApplicationUser
            {
                PasswordHash = user.PasswordHash,
                SecurityStamp = user.SecurityStamp,
                UserName = user.UserName,
                Email = user.Email,
                EmailConfirmed = user.EmailConfirmed
            };
        }

        private IdentityUser ToIdentityUser(ApplicationUser user)
        {
            return new IdentityUser
            {
                PasswordHash = user.PasswordHash,
                SecurityStamp = user.SecurityStamp,
                UserName = user.UserName,
                Email = user.Email,
                EmailConfirmed = user.EmailConfirmed
            };
        }

        private void SetApplicationUser(ApplicationUser user, IdentityUser identityUser)
        {
            user.PasswordHash = identityUser.PasswordHash;
            user.SecurityStamp = identityUser.SecurityStamp;
            user.Email = identityUser.Email;
            user.EmailConfirmed = identityUser.EmailConfirmed;
            user.UserName = identityUser.UserName;
        }
    }
}