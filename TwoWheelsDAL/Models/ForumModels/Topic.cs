﻿using System;
using System.Collections.Generic;

using FluentValidation;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.Models.ForumModels
{
    public class Topic : IEntityBase
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }

        public string UserId { get; set; }
        
        public List<string> Posts { get; set; }
        public List<Tag> Tags { get; set; }

        public Topic()
        {
        }

        public Topic(string name, string description, string userId)
        {
            Name = name;
            Description = description;
            UserId = userId;
            StartDate = DateTime.Now;
            Posts = new List<string>();
        }

        public Topic(string name, string description, string userId, List<string> tags) : this(name, description, userId)
        {
            Tags = new List<Tag>();

            foreach (var tag in tags)
            {
                Tags.Add(new Tag { Name = tag});
            }
        }
    }

    public class TopicValidator : AbstractValidator<Topic>
    {
        public TopicValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MinimumLength(10)
                .WithMessage("You must enter the topic name, and it must have minimum 10 signs");
            RuleFor(x => x.UserId).NotEmpty();
        }
    }
}