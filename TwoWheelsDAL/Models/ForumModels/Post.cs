﻿using System;
using System.Linq;

using FluentValidation;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

using TwoWheelsDAL.Infrastructure;
using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Models.AccountModels;
using TwoWheelsDAL.Repository;

namespace TwoWheelsDAL.Models.ForumModels
{
    public class Post : IEntityBase
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        public string Content { get; set; }
        public DateTime PostDate { get; set; }
        public DateTime EditDate { get; set; }
        public bool IsEdited { get; set; }

        public string UserId { get; set; }

        public Post()
        {
        }

        public Post(string content, string userId)
        {
            IsEdited = false;
            PostDate = DateTime.Now;
            Content = content;
            UserId = userId;
        }
    }

    public class PostValidator : AbstractValidator<Post>
    {
        public PostValidator()
        {
            var userReposiotry = new Repository<ApplicationUser>(new ConnectionFactory());
            RuleFor(x => x.Content).NotEmpty().WithMessage("You must enter the content");
            RuleFor(x => x.UserId).NotEmpty().WithMessage("Post must be connected with user");
        }
    }
}