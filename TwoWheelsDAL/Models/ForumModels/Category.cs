﻿using System.Collections.Generic;

using FluentValidation;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.Models.ForumModels
{
    public class Category : IEntityBase, ICategory
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        public string Name { get; set; }

        public List<string> Topics { get; set; }
        public List<string> SubCategories { get; set; }

        public Category()
        {
        }

        public Category(string name)
        {
            Name = name;
            Topics = new List<string>();
            SubCategories = new List<string>();
        }
    }

    public class CategoryValidator : AbstractValidator<Category>
    {
        public CategoryValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("You must specify the category name");
        }
    }
}