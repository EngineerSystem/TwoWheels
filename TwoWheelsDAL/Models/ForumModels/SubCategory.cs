﻿using System.Collections.Generic;

using FluentValidation;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.Models.ForumModels
{
    public class SubCategory : IEntityBase, ICategory
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        public string Name { get; set; }

        public List<string> Topics { get; set; }

        public SubCategory()
        {
        }

        public SubCategory(string name)
        {
            Name = name;
            Topics = new List<string>();
        }
    }

    public class SubCategoryValidator : AbstractValidator<SubCategory>
    {
        public SubCategoryValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("You must specify the category name");
        }
    }
}