﻿namespace TwoWheelsDAL.Models.ForumModels
{
    public class Tag
    {
        public string Name { get; set; }
    }
}