﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.Models.ReviewModels
{
    public class GearCategory : IEntityBase
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        public string Category { get; set; }
    }
}