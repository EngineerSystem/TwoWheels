﻿using System;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.Models.ReviewModels
{
    public class Review : IEntityBase
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        [Range(1, 5)]
        public int Stars { get; set; }
        public string Content { get; set; }
        public string UserId { get; set; }
        public DateTime Date { get; set; }

        public Review()
        {
            Date = DateTime.Now;
        }

        public Review(int stars, string content, string userId)
        {
            Date = DateTime.Now;
            Stars = stars;
            Content = content;
            UserId = userId;
        }
    }
}