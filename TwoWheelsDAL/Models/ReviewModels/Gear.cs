﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

using Newtonsoft.Json;

using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.Models.ReviewModels
{
    public class Gear : IReviewable
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        public string Mark { get; set; }
        [JsonProperty("Model")]
        public string Product { get; set; }
        public string GearCategoryId { get; set; }
        public List<string> Reviews { get; set; }

        public Gear()
        {
            Reviews = new List<string>();
        }
    }
}