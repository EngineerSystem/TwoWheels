﻿using System;
using System.ComponentModel.DataAnnotations;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.Models.MotorcycleModels
{
    public class Refuel : IEntityBase
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        public decimal FuelUnitPrice { get; set; }
        public decimal RefuelingPrice { get; set; }
        public float FuelCapacity { get; set; }
        public float LastFuelConsumption { get; set; }
        public int CurrentMileage { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public Refuel()
        {
            
        }

        public Refuel(int currentMileage, decimal? fuelUnitPrice, decimal? refuelingPrice, float? fuelCapacity)
        {
            Date = DateTime.Now;
            CurrentMileage = currentMileage;

            if (fuelUnitPrice == null || fuelUnitPrice.Equals(default(decimal)))
            {
                FuelCapacity = fuelCapacity.Value;
                RefuelingPrice = refuelingPrice.Value;
                FuelUnitPrice = (decimal)((float)RefuelingPrice / FuelCapacity);
            }
            else if (refuelingPrice == null || refuelingPrice.Equals(default(decimal)))
            {
                FuelCapacity = fuelCapacity.Value;
                FuelUnitPrice = fuelUnitPrice.Value;
                RefuelingPrice = FuelUnitPrice * (decimal)FuelCapacity;
            }
            else
            {
                RefuelingPrice = refuelingPrice.Value;
                FuelUnitPrice = fuelUnitPrice.Value;
                FuelCapacity = (float)RefuelingPrice / (float)FuelUnitPrice;
            }
        }
    }
}