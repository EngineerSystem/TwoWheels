﻿using System.Collections.Generic;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.Models.MotorcycleModels
{
    public class UserMotorcycle : IEntityBase
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        public string UserId { get; set; }
        public string MotorcycleModelId { get; set; }
        public string Name { get; set; }
        public int InitialMileage { get; set; }
        public int CurrentMileage { get; set; }
        public float LastTankFuelConsumption { get; set; }
        public float AverageFuelConsumption { get; set; }
        public List<OperatingPart> OperatingParts { get; set; }
        public List<string> RefuelingIds { get; set; }

        public UserMotorcycle(string userId, string motorcycleModelId, string name, int initialMileage)
        {
            UserId = userId;
            MotorcycleModelId = motorcycleModelId;
            Name = name;
            InitialMileage = CurrentMileage = initialMileage;
            LastTankFuelConsumption = 0;
            AverageFuelConsumption = 0;
            OperatingParts = new List<OperatingPart>();
            RefuelingIds = new List<string>();
        }
    }
}