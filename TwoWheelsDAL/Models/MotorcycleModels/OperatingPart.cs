﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TwoWheelsDAL.Models.MotorcycleModels
{
    public class OperatingPart
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
        public string Name { get; set; }
        public int LifeSpanInKilometers { get; set; }
        public int LastChange { get; set; }
        public float UsePercentage { get; set; }

        #region StandardValuesConstructors

        public OperatingPart()
        {
            
        }

        private OperatingPart(string name, MotorcycleType type)
        {
            ObjectId = MongoDB.Bson.ObjectId.GenerateNewId().ToString();
            Name = name;

            var rawName = name.ToLower().Trim(' ').Replace(" ", string.Empty);

            switch (rawName)
            {
                case "engineoil":
                    switch (type)
                    {
                        case MotorcycleType.Sport:
                            LifeSpanInKilometers = 5000;
                            break;
                        case MotorcycleType.Tourer:
                            LifeSpanInKilometers = 6000;
                            break;
                        case MotorcycleType.Cross:
                            LifeSpanInKilometers = 3000;
                            break;
                        case MotorcycleType.Chopper:
                            LifeSpanInKilometers = 6000;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(type), type, null);
                    }
                    break;
                case "chain":
                    switch (type)
                    {
                        case MotorcycleType.Sport:
                            LifeSpanInKilometers = 10000;
                            break;
                        case MotorcycleType.Tourer:
                            LifeSpanInKilometers = 15000;
                            break;
                        case MotorcycleType.Cross:
                            LifeSpanInKilometers = 8000;
                            break;
                        case MotorcycleType.Chopper:
                            LifeSpanInKilometers = 15000;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(type), type, null);
                    }
                    break;
                case "tires":
                    switch (type)
                    {
                        case MotorcycleType.Sport:
                            LifeSpanInKilometers = 5000;
                            break;
                        case MotorcycleType.Tourer:
                            LifeSpanInKilometers = 10000;
                            break;
                        case MotorcycleType.Cross:
                            LifeSpanInKilometers = 5000;
                            break;
                        case MotorcycleType.Chopper:
                            LifeSpanInKilometers = 10000;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(type), type, null);
                    }
                    break;
            }
        }

        public OperatingPart(string name, MotorcycleType type, int lastChange) : this(name, type)
        {
            LastChange = lastChange;
            UsePercentage = (float)LastChange / LifeSpanInKilometers;
        }

        public OperatingPart(string name, MotorcycleType type, float usePercentage) : this(name, type)
        {
            UsePercentage = usePercentage;
            LastChange = (int)UsePercentage * LifeSpanInKilometers;
        }

        #endregion

        #region UserDefinedPartsContructors

        public OperatingPart(string name, int lifeSpanInKilometers, int lastChange)
        {
            Name = name;
            LifeSpanInKilometers = lifeSpanInKilometers;
            LastChange = lastChange;
        }

        public OperatingPart(string name, int lifeSpanInKilometers, float usePercentage)
        {
            Name = name;
            LifeSpanInKilometers = lifeSpanInKilometers;
            UsePercentage = usePercentage;
        }

        #endregion
    }
}