﻿using System.Collections.Generic;

using FluentValidation;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Models.ForumModels;

namespace TwoWheelsDAL.Models.MotorcycleModels
{
    public enum MotorcycleType
    {
        None,
        Sport,
        Tourer,
        Cross,
        Chopper,
    }

    public class MotorcycleModel : IReviewable
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }

        public string FullName => $"{Mark} {Product}";

        public string Mark { get; set; }

        [JsonProperty("Model")]
        public string Product { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [BsonRepresentation(BsonType.String)]
        public MotorcycleType Type { get; set; }

        public int ProductionYear { get; set; }
        public List<string> Reviews { get; set; }
    }
}