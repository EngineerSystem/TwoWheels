﻿using System;

namespace TwoWheelsDAL.Models.AccountModels
{
    public class RefreshTokenModel
    {
        public string UserName { get; set; }

        public string UserId { get; set; }

        public DateTime IssuedDate { get; set; }

        public DateTime ExpiresDate { get; set; }

        public string ProtectedTicket { get; set; }

        public string Token { get; set; }
    }
}