﻿using AspNet.Identity.MongoDB;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.Models.AccountModels
{
    public class ApplicationRole : IdentityRole, IEntityBase
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string ObjectId { get; set; }
    }
}