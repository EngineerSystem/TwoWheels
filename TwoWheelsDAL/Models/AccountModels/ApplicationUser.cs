﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.AspNet.Identity;
using MongoDB.Bson;

namespace TwoWheelsDAL.Models.AccountModels
{
    public class ApplicationUser : IdentityUser
    {
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string PasswordHash { get; set; }
        
        public string SecurityStamp { get; set; }

        public bool EmailConfirmed { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public List<string> Motorcycles { get; set; }

        public List<string> Reviews { get; set; }

        public List<string> Topics { get; set; }

        public List<string> Posts { get; set; }



        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);

            return userIdentity;
        } 
    }

    public class ApplicationUserValidator : AbstractValidator<ApplicationUser>
    {
        public ApplicationUserValidator()
        {
            RuleFor(x => x.Email).NotEmpty().EmailAddress().WithMessage("You must enter correct e-mail address");
        }
    }
}