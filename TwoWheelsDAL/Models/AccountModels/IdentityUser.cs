﻿using Microsoft.AspNet.Identity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.IdGenerators;

using TwoWheelsDAL.Interfaces;

namespace TwoWheelsDAL.Models.AccountModels
{
    public class IdentityUser : IUser<string>, IEntityBase
    {
        public string ObjectId { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }

        public IdentityUser()
        {
            var id = MongoDB.Bson.ObjectId.GenerateNewId();
            ObjectId = Id = id.ToString();
        }
    }
}