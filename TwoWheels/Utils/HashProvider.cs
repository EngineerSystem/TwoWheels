﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace TwoWheels.Utils
{
    public class HashProvider
    {
        public static string Get(string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();
            byte[] byteValue = Encoding.UTF8.GetBytes(input);
            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);
            return Convert.ToBase64String(byteHash);
        }
    }
}