﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Models.AccountModels;

namespace TwoWheels
{
    public class ApplicationIdentityContext
    {
        private readonly IRepository<ApplicationUser> _userRepository;
        private readonly IRepository<ApplicationRole> _roleRepository;

        public ApplicationIdentityContext(IRepository<ApplicationUser> userRepository, IRepository<ApplicationRole> roleRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }

        public async Task<List<ApplicationUser>> GetAllUsers()
        {
            return await _userRepository.GetAll();
        }

        public async Task<List<ApplicationRole>> GetAllRoles()
        {
            return await _roleRepository.GetAll();
        }
    }
}