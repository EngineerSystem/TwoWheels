﻿using System.Web.Http;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Microsoft.AspNet.Identity;
using TwoWheelsDAL.Infrastructure;
using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Models;
using TwoWheelsDAL.Models.AccountModels;
using TwoWheelsDAL.Models.MotorcycleModels;
using TwoWheelsDAL.Models.ReviewModels;
using TwoWheelsDAL.Repository;
using TwoWheelsDAL.Services.AccountServices;
using TwoWheelsDAL.Services.MotorcycleServices;
using TwoWheelsDAL.Services.ReviewServices;

namespace TwoWheels.Windsor
{
    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining(typeof(IEntityBase)).BasedOn<IEntityBase>());
            
            //// Accounts Dependencies
            container.Register(Component.For<IRepository<IdentityUser>>().ImplementedBy<Repository<IdentityUser>>().LifestylePerWebRequest());

            container.Register(Component.For<IRepository<ApplicationUser>>().ImplementedBy<Repository<ApplicationUser>>().LifestylePerWebRequest());
            
            container.Register(Component.For<ApplicationUserStore>().ImplementedBy<ApplicationUserStore>());

            container.Register(
                Component.For<UserManager<ApplicationUser>>().ImplementedBy<UserManager<ApplicationUser>>());

            container.Register(Component.For<ITokenService>().ImplementedBy<TokenService>());

//            var motorcycleModuleService = new MotorcycleModuleService(container.Resolve<IRepository<MotorcycleModel>>(),
//                container.Resolve<IRepository<UserMotorcycle>>());
//
//            container.Register(Component.For<MotorcycleModuleService>().Instance(motorcycleModuleService));

            var connectionFactory = new ConnectionFactory();
            container.Register(Component.For<ConnectionFactory>().Instance(connectionFactory).LifestyleSingleton());

            var motorcycleModelRepository = new Repository<MotorcycleModel>(connectionFactory);
            container.Register(
                Component.For<IRepository<MotorcycleModel>>()
                    .Instance(motorcycleModelRepository)
                    .LifestylePerWebRequest());

            var userMotorcycleRepository = new Repository<UserMotorcycle>(connectionFactory);
            container.Register(
                Component.For<IRepository<UserMotorcycle>>().Instance(userMotorcycleRepository).LifestylePerWebRequest());

            var refuelRepository = new Repository<Refuel>(connectionFactory);
            container.Register(Component.For<IRepository<Refuel>>().Instance(refuelRepository).LifestylePerWebRequest());

            var motorcycleModuleService = new MotorcycleModuleService(
                motorcycleModelRepository,
                userMotorcycleRepository,
                refuelRepository);
            container.Register(
                Component.For<MotorcycleModuleService>().Instance(motorcycleModuleService).LifestylePerWebRequest());

            var reviewRepository = new Repository<Review>(connectionFactory);
            container.Register(Component.For<IRepository<Review>>().Instance(reviewRepository).LifestylePerWebRequest());

            var gearRepository = new Repository<Gear>(connectionFactory);
            container.Register(Component.For<IRepository<Gear>>().Instance(gearRepository).LifestylePerWebRequest());

            var gearCategoryRepository = new Repository<GearCategory>(connectionFactory);
            container.Register(
                Component.For<IRepository<GearCategory>>().Instance(gearCategoryRepository).LifestylePerWebRequest());

            var reviewModuleService = new ReviewModuleServices(reviewRepository, gearCategoryRepository, gearRepository,
                motorcycleModelRepository);
            container.Register(
                Component.For<ReviewModuleServices>().Instance(reviewModuleService).LifestylePerWebRequest());


            container.Register(Classes.FromThisAssembly()
                .BasedOn<ApiController>()
                .LifestylePerWebRequest());
        }
    }
}