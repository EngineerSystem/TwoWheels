﻿using System.Threading.Tasks;
using System.Web.Http;
using TwoWheelsDAL.Models.ReviewModels;

namespace TwoWheels.Controllers.ReviewControllers
{
    [RoutePrefix("api/Gear")]
    public class GearController : BaseApiController<Gear>
    {

        #region Overrided

        [Route("")]
        public override Task<IHttpActionResult> Get()
        {
            return base.Get();
        }

        [Route("{id}")]
        public override Task<IHttpActionResult> Get(string id)
        {
            return base.Get(id);
        }

        [Route("")]
        public override Task<IHttpActionResult> Post(Gear entity)
        {
            return base.Post(entity);
        }

        [Route("{id}")]
        public override Task<IHttpActionResult> Put(string id, Gear entity)
        {
            return base.Put(id, entity);
        }

        [Route("{id}")]
        public override Task<IHttpActionResult> Delete(string id)
        {
            return base.Delete(id);
        }

        #endregion
    }
}
