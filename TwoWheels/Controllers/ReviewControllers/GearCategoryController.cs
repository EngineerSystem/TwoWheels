﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using TwoWheelsDAL.Models.MotorcycleModels;
using TwoWheelsDAL.Models.ReviewModels;

namespace TwoWheels.Controllers.ReviewControllers
{
    [RoutePrefix("api/GearCategory")]
    public class GearCategoryController : BaseApiController<GearCategory>
    {
        #region Overrided

        [Route("")]
        [ResponseType(typeof(List<GearCategory>))]
        public override Task<IHttpActionResult> Get()
        {
            return base.Get();
        }

        [Route("{id}")]
        [ResponseType(typeof(GearCategory))]
        public override Task<IHttpActionResult> Get(string id)
        {
            return base.Get(id);
        }

        [Route("")]
        [ResponseType(typeof(GearCategory))]
        public override Task<IHttpActionResult> Post(GearCategory entity)
        {
            return base.Post(entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(int))]
        public override Task<IHttpActionResult> Put(string id, GearCategory entity)
        {
            return base.Put(id, entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(int))]
        public override Task<IHttpActionResult> Delete(string id)
        {
            return base.Delete(id);
        }

        #endregion
    }
}
