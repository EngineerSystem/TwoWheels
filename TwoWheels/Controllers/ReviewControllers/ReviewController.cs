﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Models.ReviewModels;

namespace TwoWheels.Controllers.ReviewControllers
{   
    [RoutePrefix("api/Review")]
    public class ReviewController : BaseApiController<Review>
    {
        #region Overrided

        [Route("")]
        [ResponseType(typeof(List<Review>))]
        public override Task<IHttpActionResult> Get()
        {
            return base.Get();
        }

        [Route("{id}")]
        [ResponseType(typeof(Review))]
        public override Task<IHttpActionResult> Get(string id)
        {
            return base.Get(id);
        }

        [Route("")]
        [ResponseType(typeof(Review))]
        public override Task<IHttpActionResult> Post(Review entity)
        {
            return base.Post(entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(bool))]
        public override Task<IHttpActionResult> Put(string id, Review entity)
        {
            return base.Put(id, entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(bool))]
        public override Task<IHttpActionResult> Delete(string id)
        {
            return base.Delete(id);
        }

        #endregion

    }
}
