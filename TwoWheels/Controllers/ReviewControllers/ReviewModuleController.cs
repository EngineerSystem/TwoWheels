﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TwoWheelsDAL.BindingModels.ReviewModule;
using TwoWheelsDAL.Infrastructure;
using TwoWheelsDAL.Models.MotorcycleModels;
using TwoWheelsDAL.Models.ReviewModels;
using TwoWheelsDAL.Repository;
using TwoWheelsDAL.Services.ReviewServices;

namespace TwoWheels.Controllers.ReviewControllers
{
    [RoutePrefix("api/ReviewModule")]
    public class ReviewModuleController : ApiController
    {
        private readonly ReviewModuleServices _reviewModuleService;

        public ReviewModuleController()
        {
            var connectionFactory = new ConnectionFactory();
            _reviewModuleService = new ReviewModuleServices(new Repository<Review>(connectionFactory),
                new Repository<GearCategory>(connectionFactory), new Repository<Gear>(connectionFactory),
                new Repository<MotorcycleModel>(connectionFactory));
        }

        [HttpGet]
        [Route("ObjectsToReview")]
        [ResponseType(typeof(ObjectsToReviewBindingModel))]
        public async Task<IHttpActionResult> GetObjectsToReview()
        {
            var objects = await _reviewModuleService.GetObjectsToReview();

            return Ok(objects);
        }
        
        [HttpGet]
        [Route("ObjectReviews/{id}")]
        [ResponseType(typeof(ObjectReviewsBindingModel))]
        public async Task<IHttpActionResult> GetObjectReviews(string id)
        {
            var reviews = await _reviewModuleService.GetObjectReviews(id);

            return Ok(reviews);
        }

        [HttpGet]
        [Route("CreateGear")]
        [ResponseType(typeof(CreateGearBindingModel))]
        public async Task<IHttpActionResult> GetCreateGearBindingModel()
        {
            var model = await _reviewModuleService.GetCreateGearBindingModel();

            return Ok(model);
        }

        [HttpGet]
        [Route("IndexPage")]
        [ResponseType(typeof(ReviewMainBindingModel))]
        public async Task<IHttpActionResult> GetIndexPage()
        {
            var model = await _reviewModuleService.GetIndexPageAsync();

            return Ok(model);
        }

        [HttpGet]
        [Route("CategoryId/{name}")]
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> GetGearCategoryId(string name)
        {
            var id = await this._reviewModuleService.GetCategoryId(name);

            return this.Ok(id);
        }

        [HttpPost]
        [Route("CreateReview")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> CreateReview(CreateReviewBindingModel model)
        {
            var createResult = await this._reviewModuleService.CreateReviewAsync(model);

            return this.Ok(createResult);
        }
    }
}
