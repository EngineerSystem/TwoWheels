﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TwoWheelsDAL.Models.ForumModels;

namespace TwoWheels.Controllers.ForumControllers
{
    [RoutePrefix("api/Post")]
    public class PostController : BaseApiController<Post>
    {
        #region Overrided

        [Route("")]
        [ResponseType(typeof(List<Post>))]
        public override Task<IHttpActionResult> Get()
        {
            return base.Get();
        }

        [Route("{id}")]
        [ResponseType(typeof(Post))]
        public override Task<IHttpActionResult> Get(string id)
        {
            return base.Get(id);
        }

        [Route("")]
        [ResponseType(typeof(Post))]
        public override Task<IHttpActionResult> Post(Post entity)
        {
            entity.IsEdited = false;
            entity.PostDate = DateTime.Now;
            return base.Post(entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(int))]
        public override Task<IHttpActionResult> Put(string id, Post entity)
        {
            entity.IsEdited = true;
            entity.EditDate = DateTime.Now;
            return base.Put(id, entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(int))]
        public override Task<IHttpActionResult> Delete(string id)
        {
            return base.Delete(id);
        }

        #endregion



    }
}
