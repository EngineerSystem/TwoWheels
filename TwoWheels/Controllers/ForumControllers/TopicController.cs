﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TwoWheelsDAL.Models.ForumModels;

namespace TwoWheels.Controllers.ForumControllers
{
    [RoutePrefix("api/Topic")]
    public class TopicController : BaseApiController<Topic>
    {
        #region Overrided

        [Route("")]
        [ResponseType(typeof(List<Topic>))]
        public override Task<IHttpActionResult> Get()
        {
            return base.Get();
        }

        [Route("{id}")]
        [ResponseType(typeof(Topic))]
        public override Task<IHttpActionResult> Get(string id)
        {
            return base.Get(id);
        }

        [Route("")]
        [ResponseType(typeof(Topic))]
        public override Task<IHttpActionResult> Post(Topic entity)
        {
            return base.Post(entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(int))]
        public override Task<IHttpActionResult> Put(string id, Topic entity)
        {
            return base.Put(id, entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(int))]
        public override Task<IHttpActionResult> Delete(string id)
        {
            return base.Delete(id);
        }

        #endregion
    }
}