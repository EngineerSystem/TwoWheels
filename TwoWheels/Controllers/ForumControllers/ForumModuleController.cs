﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TwoWheelsDAL.BindingModels.ForumModule;
using TwoWheelsDAL.Infrastructure;
using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Models.AccountModels;
using TwoWheelsDAL.Models.ForumModels;
using TwoWheelsDAL.Repository;
using TwoWheelsDAL.Services.ForumServices;

namespace TwoWheels.Controllers.ForumControllers
{
    [RoutePrefix("api/ForumModule")]
    public class ForumModuleController : ApiController
    {
        private readonly ForumModuleService _forumModuleService;

        public ForumModuleController()
        {
            var connection = new ConnectionFactory();
            var topicRepo = new Repository<Topic>(connection);
            var postRepo = new Repository<Post>(connection);
            var categoryRepo = new Repository<Category>(connection);
            var subCategoryRepo = new Repository<SubCategory>(connection);
            var userRepo = new Repository<ApplicationUser>(connection);

            _forumModuleService = new ForumModuleService(topicRepo, postRepo, categoryRepo, subCategoryRepo, userRepo);
        }

        [HttpGet]
        [Route("Index")]
        [ResponseType(typeof(ForumMainPageBindingModel))]
        public async Task<IHttpActionResult> GetForumMainPageBindingModel()
        {
            var model = await _forumModuleService.GetForumMailPageBindingModel();

            return Ok(model);
        }

        [HttpGet]
        [Route("GetCategoryTopics/{id}")]
        [ResponseType(typeof(CategoryTopicsBindingModel))]
        public async Task<IHttpActionResult> GetCategoryTopics(string id)
        {
            var topics = await _forumModuleService.GetCategoryTopics(id);

            return Ok(topics);
        }

        [HttpGet]
        [Route("GetTopicContent/{id}")]
        [ResponseType(typeof(TopicContentBindingModel))]
        public async Task<IHttpActionResult> GetTopicContent(string id)
        {
            var content = await _forumModuleService.GetTopicContent(id);

            return Ok(content);
        }

        [HttpPost]
        [Route("CreateTopic")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> CreateTopic([FromBody]CreateTopicBindingModel model)
        {
            var createResult = await _forumModuleService.CreateTopic(model);

            return Ok(createResult);
        }

        [HttpPost]
        [Route("CreatePost")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> CreatePost([FromBody]CreatePostBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest("Model was not valid");
            }

            var createResult = await this._forumModuleService.CreatePost(model);

            return Ok(createResult);
        }

        [HttpPost]
        [Route("CreateCategory/{id?}")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> CreateCategory([FromBody]Category model, [FromUri]string id = "")
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest("Model was not valid!");
            }

            var createResult = await this._forumModuleService.CreateCategory(model, id);

            return this.Ok(createResult);
        }

        [HttpGet]
        [Route("SearchByTag/{id}")]
        [ResponseType(typeof(SearchedTopicsBindingModel))]
        public async Task<IHttpActionResult> SearchByTag(string id)
        {
            var topics = await this._forumModuleService.SearchByTag(id);

            return this.Ok(topics);
        }
    }
}
