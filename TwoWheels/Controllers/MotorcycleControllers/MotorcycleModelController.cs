﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TwoWheelsDAL.Models;
using TwoWheelsDAL.Models.MotorcycleModels;

namespace TwoWheels.Controllers.MotorcycleControllers
{
    [RoutePrefix("api/MotorcycleModel")]
    [Authorize]
    public class MotorcycleModelController : BaseApiController<MotorcycleModel>
    {
        #region Overrided

        [Route("")]
        [ResponseType(typeof(List<MotorcycleModel>))]
        public override Task<IHttpActionResult> Get()
        {
            return base.Get();
        }

        [Route("{id}")]
        [ResponseType(typeof(MotorcycleModel))]
        public override Task<IHttpActionResult> Get(string id)
        {
            return base.Get(id);
        }

        [Route("")]
        [ResponseType(typeof(MotorcycleModel))]
        public override Task<IHttpActionResult> Post(MotorcycleModel entity)
        {
            return base.Post(entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(int))]
        public override Task<IHttpActionResult> Put(string id, MotorcycleModel entity)
        {
            return base.Put(id, entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(int))]
        public override Task<IHttpActionResult> Delete(string id)
        {
            return base.Delete(id);
        }

        #endregion
    }
}