﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using TwoWheelsDAL.BindingModels.MotorcycleModule;
using TwoWheelsDAL.Infrastructure;
using TwoWheelsDAL.Models;
using TwoWheelsDAL.Models.MotorcycleModels;
using TwoWheelsDAL.Repository;
using TwoWheelsDAL.Services.MotorcycleServices;

namespace TwoWheels.Controllers.MotorcycleControllers
{
    [RoutePrefix("api/MotorcycleModule")]
    public class MotorcycleModuleController : ApiController
    {
        private readonly MotorcycleModuleService _motorcycleModuleService;

        public MotorcycleModuleController()
        {
            var connectionFactory = new ConnectionFactory();
            _motorcycleModuleService =
                new MotorcycleModuleService(new Repository<MotorcycleModel>(connectionFactory),
                    new Repository<UserMotorcycle>(connectionFactory),
                    new Repository<Refuel>(connectionFactory));
        }

        [HttpGet]
        [Route("Get/{id}")]
        [ResponseType(typeof(List<MotorcycleBindingModel>))]
        public async Task<IHttpActionResult> Get(string id)
        {
            var userMotorcycles = await _motorcycleModuleService.GetUserMotorcycles(id);

            return Ok(userMotorcycles);
        }

        [HttpGet]
        [Route("GetType/{id}")]
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> GetUserMotorcycleType(string id)
        {
            var type = await _motorcycleModuleService.GetUserMotorcycleType(id);

            return Ok(type);
        }

        [HttpGet]
        [Route("GetDetails/{id}")]
        [ResponseType(typeof(MotorcycleDetailsBindingModel))]
        public async Task<IHttpActionResult> GetMotorcycleDetails(string id)
        {
            var details = await _motorcycleModuleService.GetMotorcycleDetails(id);

            return Ok(details);
        }

        [HttpPost]
        [Route("CreateUserMotorcycle")]
        [ResponseType(typeof(UserMotorcycle))]
        public async Task<IHttpActionResult> CreateUserMotorcycle(UserMotorcycle model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Model was not valid");
            }

            var result = await _motorcycleModuleService.CreateUserMotorcycle(model);

            return Ok(result);
        }

        [HttpPost]
        [Route("CreatePart")]
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> CreatePart([FromBody]CreateOperatingPartBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Model was not valid");
            }

            var createResult = await _motorcycleModuleService.CreateOperatingPart(model);

            return Ok(createResult);
        }

        [HttpPost]
        [Route("UpdateParts")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> UpdateOperatingParts(ManageOperatingPartsBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Model was not valid");
            }

            var updateResult = await _motorcycleModuleService.UpdateOperatingParts(model);

            return Ok(updateResult);
        }

        [HttpGet]
        [Route("GetParts/{id}")]
        [ResponseType(typeof(ManageOperatingPartsBindingModel))]
        public async Task<IHttpActionResult> GetUserMotorcycleOperatingParts(string id)
        {
            var parts = await _motorcycleModuleService.GetUserMotorcycleOperatingParts(id);

            return Ok(parts);
        }

        [HttpPost]
        [Route("AddRefueling")]
        [ResponseType(typeof(float))]
        public async Task<IHttpActionResult> AddRefueling(AddRefuelingBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Model was not valid");
            }

            var addResult = await _motorcycleModuleService.AddRefueling(model);

            return Ok(addResult);
        }

        [HttpGet]
        [Route("GetRefuelings/{id}")]
        [ResponseType(typeof(List<Refuel>))]
        public async Task<IHttpActionResult> GetRefuelings(string id)
        {
            var refuelings = await this._motorcycleModuleService.GetRefuelings(id);

            return Ok(refuelings);
        }

        [HttpPost]
        [Route("ChangePart")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> ChangePart(ChangePartBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Request was not valid!");
            }

            var result = await _motorcycleModuleService.ChangePart(model);

            return Ok(result);
        }
    }
}
