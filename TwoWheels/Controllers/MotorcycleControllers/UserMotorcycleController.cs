﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TwoWheelsDAL.Models;
using TwoWheelsDAL.Models.ForumModels;
using TwoWheelsDAL.Models.MotorcycleModels;

namespace TwoWheels.Controllers.MotorcycleControllers
{
    [RoutePrefix("api/UserMotorcycle")]
    public class UserMotorcycleController : BaseApiController<UserMotorcycle>
    {
        #region Overrided

        [Route("")]
        [ResponseType(typeof(List<UserMotorcycle>))]
        public override Task<IHttpActionResult> Get()
        {
            return base.Get();
        }

        [Route("{id}")]
        [ResponseType(typeof(UserMotorcycle))]
        public override Task<IHttpActionResult> Get(string id)
        {
            return base.Get(id);
        }

        [Route("")]
        [ResponseType(typeof(UserMotorcycle))]
        public override Task<IHttpActionResult> Post(UserMotorcycle entity)
        {
            return base.Post(entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(int))]
        public override Task<IHttpActionResult> Put(string id, UserMotorcycle entity)
        {
            return base.Put(id, entity);
        }

        [Route("{id}")]
        [ResponseType(typeof(int))]
        public override Task<IHttpActionResult> Delete(string id)
        {
            return base.Delete(id);
        }

        #endregion
    }
}