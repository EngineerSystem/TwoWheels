﻿using System.Threading.Tasks;
using System.Web.Http;
using TwoWheelsDAL.Infrastructure;
using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Repository;

namespace TwoWheels.Controllers
{
    public abstract class BaseApiController<TEntity> : ApiController where TEntity : IEntityBase
    {
        private readonly IRepository<TEntity> _repository;

        protected BaseApiController()
        {
            _repository = new Repository<TEntity>(new ConnectionFactory());
        }
        
        [HttpGet]
        public virtual async Task<IHttpActionResult> Get()
        {
            var result = await _repository.GetAll();
            return Ok(result);
        }

        [HttpGet]
        public virtual async Task<IHttpActionResult> Get(string id)
        {
            var entity = await _repository.Get(id);

            return Ok(entity);
        }

        [HttpPost]
        public virtual async Task<IHttpActionResult> Post([FromBody] TEntity entity)
        {
            var postResult = await _repository.Add(entity);

            return Ok(postResult);
        }

        [HttpPut]
        public virtual async Task<IHttpActionResult> Put([FromUri] string id, [FromBody] TEntity entity)
        {
            var result = await _repository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            var updateResult = await _repository.Update(x => x.ObjectId.ToString().Equals(id), entity);

            return Ok(updateResult);
        }

        [HttpDelete]
        public virtual async Task<IHttpActionResult> Delete([FromUri] string id)
        {
            var result = await _repository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            var deleteResult = await _repository.Delete(result);

            return Ok(deleteResult.DeletedCount);
        }
    }
}