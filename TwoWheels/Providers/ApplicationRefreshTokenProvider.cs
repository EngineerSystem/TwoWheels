﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Owin.Security.Infrastructure;
using TwoWheels.Utils;
using TwoWheelsDAL.Interfaces;
using TwoWheelsDAL.Models.AccountModels;

#pragma warning disable 1998

namespace TwoWheels.Providers
{
    public class ApplicationRefreshTokenProvider : AuthenticationTokenProvider
    {
        private readonly ITokenService _tokenService;
        private readonly IRepository<ApplicationUser> _userRepository;

        public ApplicationRefreshTokenProvider(ITokenService tokenService, IRepository<ApplicationUser> userRepository)
        {
            _tokenService = tokenService;
            _userRepository = userRepository;
        }

        public override async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            string userName = context.Ticket.Identity.Name;
            var user = await _userRepository.Find(x => x.UserName == userName);

            string token = Guid.NewGuid().ToString();

            var refreshToken = new RefreshTokenModel
            {
                UserName = userName,
                UserId = user.FirstOrDefault()?.Id,
                Token = HashProvider.Get(token),
                ProtectedTicket = context.SerializeTicket(),
                IssuedDate = DateTime.Now,
                ExpiresDate = DateTime.Now.AddMinutes(AppConfiguration.RefreshTokenExpireTimeInMin)
            };

            _tokenService.SaveRefreshToken(refreshToken);

            context.Ticket.Properties.IssuedUtc = refreshToken.IssuedDate;
            context.Ticket.Properties.ExpiresUtc = refreshToken.ExpiresDate;

            context.SetToken(token);
        }

        public override async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var token = HashProvider.Get(context.Token);

            var refreshToken = _tokenService.GetRefreshToken(token);

            if (refreshToken != null)
            {
                context.DeserializeTicket(refreshToken.ProtectedTicket);

                _tokenService.RemoveRefreshToken(token);
                _tokenService.RemoveExpiredRefreshTokens(refreshToken.UserName);
            }
        }
    }
}