﻿using System.Collections.Generic;
using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace TwoWheels.Providers
{
    public class TokenEndpointDocumentFilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
        {
            swaggerDoc.paths.Add("/Token", new PathItem
            {
                post = new Operation
                {
                    tags = new List<string> { "Authentication" },
                    summary = "Authenticates provided credentials and returns token",
                    operationId = "OAuth2TokenPost",
                    consumes = new List<string> { "application/x-www-from-url-encoded" },
                    produces = new List<string> { "application/json" },
                    parameters = new List<Parameter>
                    {
                        new Parameter
                        {
                            name = "username",
                            @in = "formData",
                            type = "string"
                        },
                        new Parameter
                        {
                            name = "password",
                            @in = "formData",
                            type = "string"
                        },
                        new Parameter
                        {
                            name = "grant_type",
                            @in = "formData",
                            type = "string"
                        }
                    },
                    responses = new Dictionary<string, Response>
                    {
                        { "200", new Response
                        {
                            description = "Success",
                            schema = new Schema
                            {
                                type = "object",
                                properties = new Dictionary<string, Schema>
                                {
                                    { "access_token", new Schema{ type = "string"} },
                                    { "token_type", new Schema{type = "string"} },
                                    { "expires_in", new Schema{type = "integer"} },
                                    { "refresh_token", new Schema{type = "string"} },
                                    { "userName", new Schema{type = "string"} },
                                    { ".issued", new Schema{type = "date-time"} },
                                    { ".expires", new Schema{type = "date-time"} }
                                }
                            }
                        }},
                        { "400", new Response
                        {
                            description = "Bad Request",
                            schema = new Schema
                            {
                                type = "object",
                                properties = new Dictionary<string, Schema>
                                {
                                    { "error", new Schema{ type = "string"} },
                                    { "error_description", new Schema{ type = "string"} }
                                }
                            }
                        }}
                    }
                }
            });
        }
    }
}