﻿using System.Collections.Generic;

using FluentAssertions;

using NUnit.Framework;

using TwoWheelsDAL.Models.MotorcycleModels;
using TwoWheelsDAL.UnitOfWork;

namespace TwoWheelsTests.UnitTests
{
    [TestFixture]
    public class MotorcycleModuleTests
    {
        private List<Refuel> refuelings;
        private MotorcycleModuleUnitOfWork motorcycleModuleUnitOfWork;

        [SetUp]
        public void PrepareData()
        {
            this.motorcycleModuleUnitOfWork = new MotorcycleModuleUnitOfWork();

            this.refuelings = new List<Refuel>
            {
                new Refuel(700, (decimal)4.5, 45, null),
                new Refuel(1000, null, (decimal)67.5, 15),
                new Refuel(1200, (decimal)4.5F, null, 10),
                new Refuel(1400, (decimal)4.5F, null, 12)
            };
        }

        [Test]
        public void RecalculateAverageFuelConsumptionTest()
        {
            this.motorcycleModuleUnitOfWork.RecalculateAverageFuelConsumption(this.refuelings)
                .Should()
                .Be((float)37 / 7);
        }

        [Test]
        public void RecalculateLastFuelConsumptionTest()
        {
            this.motorcycleModuleUnitOfWork.RecalculateLastTankFuelConsumption(this.refuelings)
                .Should()
                .Be((float)12 / 2);
        }

        [Test]
        public void RecalculateOperatingPartUsageTest()
        {
            var userMotorcycle = new UserMotorcycle("100", "100", "Motorcycle", 500);
            userMotorcycle.OperatingParts.Add(new OperatingPart("Engine Oil", MotorcycleType.Sport, 0));

            var operatingParts = this.motorcycleModuleUnitOfWork.RecalculateOperatingPartUsage(userMotorcycle.OperatingParts, new Refuel(1000, (decimal)4.5, null, 30));

            operatingParts[0].UsePercentage.Should().Be(0.2F);
        }
    }
}