﻿using System.Collections.Generic;
using System.Threading.Tasks;

using FluentAssertions;

using NUnit.Framework;

using TwoWheelsDAL.Infrastructure;
using TwoWheelsDAL.Models.MotorcycleModels;
using TwoWheelsDAL.Repository;

namespace TwoWheelsTests.IntegrationTests
{
    [TestFixture]
    public class RepositoryTests
    {
        private Repository<MotorcycleModel> _motorcycleRepository;

        [SetUp]
        public void PrepareObjects()
        {
            var connectionFactory = new ConnectionFactory();
            this._motorcycleRepository = new Repository<MotorcycleModel>(connectionFactory);
        }

        [Test]
        public async Task GetMotorcycleModels_ReturnsCollection()
        {
            var motorcycleModels = await this._motorcycleRepository.GetAll();

            motorcycleModels.Should().NotBeEmpty();
        }

        [Test]
        public async Task CrudOperations()
        {
            var motorcycleModel = new MotorcycleModel
            {
                Mark = "BMW",
                Model = "1200 GS",
                ProductionYear = 2012,
                Reviews = new List<string>(),
                Type = MotorcycleType.Tourer
            };

            var addResult = await this._motorcycleRepository.Add(motorcycleModel);

            addResult.ProductionYear = 2000;

            var updateResult = await this._motorcycleRepository.Update(
                x => x.ObjectId.Equals(addResult.ObjectId),
                addResult);

            var deleteResult = await this._motorcycleRepository.Delete(addResult);

            var deletedObject = await this._motorcycleRepository.Get(addResult.ObjectId);

            addResult.ObjectId.Should().NotBeNullOrWhiteSpace();
            updateResult.ProductionYear.Should().Be(2000);
            deleteResult.DeletedCount.Should().Be(1);
            deletedObject.Should().Be(null);
        }
    }
}
